#!/data/data/com.termux/files/usr/bin/bash
# Pin Layout -> change if you are using a diffrent wiring setup
pin_up=5
pin_down=6
pin_power=3

su -c "echo $pin_up > /sys/class/gpio/export"
su -c "echo $pin_down > /sys/class/gpio/export"
su -c "echo $pin_power > /sys/class/gpio/export"

# executing the project binary, assuming you have added it to your path
~/rotary-encoder-rs/target/release/rotary-encoder-rs ~/.config/re/gpio.toml
