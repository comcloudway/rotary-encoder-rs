extern crate sysfs_gpio;

use sysfs_gpio::{Direction, Pin, Edge};
use std::{env, fs, thread::sleep, time::{Duration, SystemTime}, process::Command};
use serde_derive::Deserialize;

#[derive(Deserialize)]
struct Config {
    volume: VolumeConfig,
    power: PowerConfig, 
}
#[derive(Deserialize)]
struct VolumeConfig {
    pin_up: u64,
    pin_down: u64,
    cmd_up: String,
    cmd_down: String,
    use_clk: bool,
    cnt_up: usize,
    cnt_down: usize,
    enabled: bool,
}
#[derive(Deserialize)]
struct PowerConfig {
    pin_power: u64,
    long_press_duration: u64,
    cmd_power: String,
    cmd_power_long: String,
    enabled: bool,
}

enum Action {
    VolumeUp,
    VolumeDown,
    Power,
    PowerLongPress,
    None
}

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() != 2 {
        // SHOW HELP
        println!("To use this programm you have to sepcify a path to your gpio.toml file");
        println!("e.g. {} /home/pi/gpio.toml", &args[0]);
    } else {
        //
        // RUN PROGRAMM
        //

        // Read Config
        let filename = &args[1];
        println!("Loading configuration file from {}", filename);
        let content = fs::read_to_string(filename)
            .expect("Something went wrong loading your config file");
        println!("Loading configuration done");
        // Parse Config
        let config: Config = toml::from_str(&content).unwrap();

        // create pins
        let mut pin_up:Option<Pin> = None;
        let mut pin_down:Option<Pin> = None;
        let mut pin_power:Option<Pin> = None;
        if config.volume.enabled {
            pin_up = Some(Pin::new(config.volume.pin_up));
            pin_down = Some(Pin::new(config.volume.pin_down));
        }
        if config.power.enabled {
            pin_power = Some(Pin::new(config.power.pin_power));
        }

        // LOOP
        // setup store
        let mut last_up: u8 = 2;
        let mut last_down: u8 = 2;
        let mut last_power: u8 = 2;

        let mut cup: usize = 0;
        let mut cdown: usize = 0;

        // fix volume button being detected twice bug
        let mut pressed_up: usize = 0;
        let mut pressed_down: usize = 0;

        // power button long press timer
        let mut timer = SystemTime::now();

        loop {
            let mut act_volume: Action = Action::None;
            let mut act_power: Action = Action::None;

            let mut up:Option<u8> = None;
            let mut down:Option<u8> = None;
            let mut power:Option<u8> = None;
            // read pins
            if config.volume.enabled {
                if let Some(pin_up) = pin_up {
                up = Some(pin_up.get_value().unwrap());
                }
                if let Some(pin_down) = pin_down {
                down = Some(pin_down.get_value().unwrap());
                }
            }
            if config.power.enabled {
                if let Some(pin_power) = pin_power{
                power = Some(pin_power.get_value().unwrap());
                }
            }

            if config.volume.enabled {
                if config.volume.use_clk {
                    // Encoder has DT an CLK pin
                    if let Some(up) = up {
                    if up != last_up && last_up != 2 {
                        if last_down != up {
                            act_volume = Action::VolumeUp;
                            pressed_up+=1;
                        } else {
                            act_volume = Action::VolumeDown;
                            pressed_down+=1;
                        }
                    }
                    }
                } else {
                    // Encoder uses seperate UP and DOWN pins
                    if last_up == 0 {
                        // UP
                        act_volume = Action::VolumeUp;
                        pressed_up+=1;
                    } else {
                        // DOWN
                        act_volume = Action::VolumeDown;
                        pressed_down+=1;
                    }
                }
            }
            if config.power.enabled {
                // Handle Power button
                if let Some(power) = power {
                if power != last_power && last_power != 2 {
                    if power == 0 {
                        // press -> start timer
                        timer = SystemTime::now();
                    } else {
                        // release -> check timer -> detect long press
                        match timer.elapsed() {
                            Ok(elapsed) => {
                                if elapsed < Duration::new(config.power.long_press_duration, 0) {
                                    act_power = Action::Power;
                                } else {
                                    act_power = Action::PowerLongPress;
                                }
                            }
                            Err(e)=>{}
                        }
                    }
                }}}

            // save values
            if config.volume.enabled {
                if let Some(up) = up {
                last_up = up;
                }
                if let Some(down) = down {
                last_down = down;
                }
            }
            if config.power.enabled {
                if let Some(power) = power {
                last_power = power;
                }
            }

            // Power & Volume seperated to allow volume increase and power button press at same time
            // Detect Volume action

            match act_volume {
                Action::VolumeUp => {
                    if pressed_up % 2 == 0 {
                        cup=cup+1;
                        println!("UP");
                        if cup >= config.volume.cnt_up {
                            cup=0;
                            Command::new("sh")
                                .arg("-c")
                                .arg(&config.volume.cmd_up)
                                .output()
                                .expect("Failed to run volume.cmd_up");
                        }
                    }
                },
                Action::VolumeDown => {
                    if pressed_down % 2 == 0 {
                        cdown = cdown +1;
                        println!("DOWN");
                        if cdown >= config.volume.cnt_down {
                            cdown=0;
                            Command::new("sh")
                                .arg("-c")
                                .arg(&config.volume.cmd_down)
                                .output()
                                .expect("Failed to run volume.cmd_down");
                        }
                    }
                },
                _ => ()
            };
            // Detect Power action
            match act_power {
                Action::Power => {
                    println!("POWER");
                    Command::new("sh")
                        .arg("-c")
                        .arg(&config.power.cmd_power)
                        .output()
                        .expect("Failed to run power.cmd_power");
                },
                Action::PowerLongPress => {
                    println!("POWER LONG PRESS");
                    Command::new("sh")
                        .arg("-c")
                        .arg(&config.power.cmd_power_long.to_string())
                        .output()
                        .expect("Failed to run power.cmd_power.long");
                }
                _ => ()
            }
        }
    }
}
